<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "events_organizers".
 *
 * @property int $id
 * @property int|null $event_id
 * @property int|null $organizer_id
 *
 * @property Event $site
 * @property Organizer $organizer
 */
class EventsOrganizers extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'events_organizers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['event_id', 'organizer_id'], 'integer'],
            [['event_id'], 'exist', 'skipOnError' => true, 'targetClass' => Event::class, 'targetAttribute' => ['event_id' => 'id']],
            [['organizer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Organizer::class, 'targetAttribute' => ['organizer_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'event_id' => 'Event ID',
            'organizer_id' => 'Organizer ID',
        ];
    }

    /**
     * Gets query for [[Event]].
     *
     * @return ActiveQuery
     */
    public function getEvent(): ActiveQuery
    {
        return $this->hasOne(Event::class, ['id' => 'event_id']);
    }

    /**
     * Gets query for [[Organizer]].
     *
     * @return ActiveQuery
     */
    public function getOrganizer(): ActiveQuery
    {
        return $this->hasOne(Organizer::class, ['id' => 'organizer_id']);
    }
}
