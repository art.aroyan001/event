<?php

namespace app\models;

use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "events".
 *
 * @property int $id
 * @property string $name
 * @property string|null $date
 * @property string|null $description
 *
 * @property EventsOrganizers[] $eventsOrganizers
 */
class Event extends ActiveRecord
{

    public $organizer;

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'events';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['date', 'organizer'], 'safe'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'date' => 'Date',
            'description' => 'Description',
        ];
    }

    /**
     * @param $insert
     * @return bool
     * @throws InvalidConfigException
     */
    public function beforeSave($insert): bool
    {
        $this->date = Yii::$app->formatter->asDate($this->date, 'yyyy-MM-dd');

        parent::beforeSave($insert);

        return true;
    }

    /**
     * Gets query for [[EventsOrganizers]].
     *
     * @return ActiveQuery
     */
    public function getEventsOrganizers(): ActiveQuery
    {
        return $this->hasMany(EventsOrganizers::class, ['event_id' => 'id']);
    }

    /**
     * Get site organizers.
     *
     * @return ActiveQuery
     * @throws InvalidConfigException
     */
    public function getOrganizers(): ActiveQuery
    {
        return $this->hasMany(Organizer::class, ['id' => 'organizer_id'])
            ->viaTable('events_organizers', ['event_id' => 'id']);
    }

    /**
     * @param array $organizers
     * @return void
     * @throws \yii\db\Exception
     */
    public function setOrganizers(array $organizers)
    {
        foreach ($organizers as $id) {
            $eventsOrganizers = new EventsOrganizers();
            $eventsOrganizers->event_id = $this->id;
            $eventsOrganizers->organizer_id = $id;
            $eventsOrganizers->save();
        }
    }
}
