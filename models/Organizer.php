<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "organizers".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string|null $phone
 *
 * @property EventsOrganizers[] $eventsOrganizers
 */
class Organizer extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'organizers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['name', 'email'], 'required'],
            ['email', 'email'],
            [['name', 'email', 'phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
        ];
    }

    /**
     * Gets query for [[EventsOrganizers]].
     *
     * @return ActiveQuery
     */
    public function getEventsOrganizers(): ActiveQuery
    {
        return $this->hasMany(EventsOrganizers::class, ['organizer_id' => 'id']);
    }
}
