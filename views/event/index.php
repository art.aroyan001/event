<?php

/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <?php foreach ($events as $event) : ?>
                <div class="col-lg-4 mb-3">
                    <h2>Event</h2>
                    <h3><?= $event->name ?></h3>
                    <p><?= $event->date ?></p>

                    <p><?= $event->description ?></p>

                    <h2>Organizers:</h2>
                    <?php foreach ($event->organizers as $organizer): ?>
                        <p>Name: <?= $organizer->name ?></p>
                        <p>Email: <?= $organizer->email ?></p>
                        <p>Phone: <?= $organizer->phone ?></p>
                        <hr>
                    <?php endforeach; ?>
                </div>
            <?php endforeach; ?>
        </div>

    </div>
</div>
