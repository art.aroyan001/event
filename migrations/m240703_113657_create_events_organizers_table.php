<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%events_organizers}}`.
 */
class m240703_113657_create_events_organizers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%events_organizers}}', [
            'id' => $this->primaryKey(),
            'event_id' => $this->integer(),
            'organizer_id' => $this->integer(),
        ]);

        // creates index for column `event_id`
        $this->createIndex(
            'idx-events_organizers-event_id',
            'events_organizers',
            'event_id'
        );

        // add foreign key for table `events`
        $this->addForeignKey(
            'fk-events_organizers-event_id',
            'events_organizers',
            'event_id',
            'events',
            'id',
            'CASCADE'
        );

        // creates index for column `organizer_id`
        $this->createIndex(
            'idx-events_organizers-organizer_id',
            'events_organizers',
            'organizer_id'
        );

        // add foreign key for table `organizers`
        $this->addForeignKey(
            'fk-events_organizers-organizer_id',
            'events_organizers',
            'organizer_id',
            'organizers',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `events`
        $this->dropForeignKey(
            'fk-events_organizers-event_id',
            'events_organizers'
        );

        // drops index for column `event_id`
        $this->dropIndex(
            'idx-events_organizers-event_id',
            'events_organizers'
        );

        // drops foreign key for table `organizers`
        $this->dropForeignKey(
            'fk-events_organizers-organizer_id',
            'events_organizers'
        );

        // drops index for column `organizer_id`
        $this->dropIndex(
            'idx-events_organizers-organizer_id',
            'events_organizers'
        );

        $this->dropTable('{{%events_organizers}}');
    }
}
